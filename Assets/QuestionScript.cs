﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestionScript : MonoBehaviour {

    public string PORT_NAME;
    public string PORT_DESCRIPTION;
    public string PORT_ACRONYM;
    public List<int> PORT_TCP = new List<int>();
    public List<int> PORT_UDP = new List<int>();
    public int ANSWERED_CORRECTLY;
}
