﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QUESTION_POOLER : MonoBehaviour {
    public GameObject PREFAB_QUESTION;
    public List<QuestionScript> QUESTIONS = new List<QuestionScript>();
	// Use this for initialization
	void Start () {
        GameObject TEMP_OBJECT;
        QuestionScript TEMP_QUESTION;
	    for(int i = 0; i < UtilList.ListName.Count; i++)
        {
            TEMP_OBJECT = (GameObject) Instantiate(PREFAB_QUESTION, Vector3.zero, Quaternion.identity);
            TEMP_QUESTION = TEMP_OBJECT.GetComponent<QuestionScript>();

            TEMP_QUESTION.PORT_ACRONYM = UtilList.ListAcronym[i];
            TEMP_QUESTION.PORT_DESCRIPTION = UtilList.ListDescription[i];
            TEMP_QUESTION.PORT_NAME = UtilList.ListName[i];
            TEMP_QUESTION.PORT_TCP = UtilList.ListPortTCP[i];
            TEMP_QUESTION.PORT_UDP = UtilList.ListPortUDP[i];

            QUESTIONS.Add(TEMP_QUESTION);
        }
	}
	
	
}
